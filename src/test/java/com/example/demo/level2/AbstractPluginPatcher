package com.junli.service.apiInvoke.serviceLoader.remotePatcher;

import com.junli.api.base.RequestContent;
import com.junli.domain.dto.ServiceBaseEntity;
import com.junli.service.plugins.PluginService;
import com.junli.spring.SpringBeanFactory;
import org.springframework.beans.BeanUtils;

import java.util.Map;

/**
 * 插件包抽象类
 * Created by fei.tang on 2019/11/20.
 */
public abstract class AbstractPluginPatcher extends PatcherBase {

    @Override
    public String patch(RequestContent requestContent, ServiceBaseEntity service) {
        //准备已配置的参数
        Map<String, Object> args = prepareArgs(requestContent, service);
        PluginService pluginService = getService();
        com.junli.domain.RequestContent request = castRequest(requestContent);
        return pluginService.getPluginResult(service.getId(), request, args);
    }

    protected com.junli.domain.RequestContent castRequest(RequestContent requestContent){
        com.junli.domain.RequestContent req = new com.junli.domain.RequestContent();
        BeanUtils.copyProperties(requestContent,req);
        return req;
    }

    public abstract Map<String,Object> prepareArgs(RequestContent requestContent, ServiceBaseEntity service);

    public PluginService getService(){
        return SpringBeanFactory.getBean(PluginService.class);
    }

}
